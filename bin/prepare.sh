rm -rf app/cache/* app/logs/*

#!/bin/bash
php composer.phar install --dev

# clear, create db and load data
#create a PostrgreSQL database, uncomment next for first run
#createdb -U postgres -W lfk
php app/console doctrine:schema:drop --force
php app/console doctrine:schema:create

# load fixtures
php app/console doctrine:fixtures:load -n

# clear cache
php app/console cache:warmup -e=prod
php app/console cache:clear -e=prod

# assets install
php app/console assetic:dump
