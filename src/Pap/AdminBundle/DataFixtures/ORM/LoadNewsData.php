<?php

namespace Pap\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Pap\AdminBundle\Entity\News;

class LoadNewsData extends AbstractFixture implements OrderedFixtureInterface
{
    private $news = [
        [
            'title' => 'Первым посетителям сайта СКИДКИ',
            'brief' => 'Только у нас, при посещении нашего сайта, вы можете получить скидку на входные двери для домов и котеджей.*',
            'content' => '<p>Только у нас, при посещении нашего сайта, вы можете получить скидку на входные двери для домов и котеджей.*</p><p>Для этого вам необходимо обратиться в один из наших магазинов и назвать адрес нашего сайта www.art-freedom.by.</p><p>Скидка составляет 5%.<br />Будем рады вас видеть в наших магазинах.</p><p>_________________________________________________</p><p>* Скидки не суммируются.</p>',
        ]
    ];

    public function load(ObjectManager $manager)
    {
        foreach ($this->news as $one) {
            $news = (new News())
                ->setTitle($one['title'])
                ->setBrief($one['brief'])
                ->setDescription($one['content'])
            ;
            $manager->persist($news);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 11;
    }
}
