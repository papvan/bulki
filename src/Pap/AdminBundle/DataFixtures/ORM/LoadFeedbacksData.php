<?php

namespace Pap\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Pap\AdminBundle\Entity\Feedback;

class LoadFeedbacksData extends AbstractFixture implements OrderedFixtureInterface
{
    private $feedbacks = [
        [
            'name' => 'Андрей Андреевич',
            'email' => 'example@mail.com',
            'content' => 'Mauris in erat justo. Nullam ac urna eu felis dapibus condimentumi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus rhoncus alesuada. Pellentesque molestie lacus nec ligula rhoncus pretium. Fusce id nulla nunc. Vivamus vulputate mauris eu mi accumsan accumsan eu nec nisi. Nalla non dolor lacinia, rutrum est a, pharetra massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pallentesque pellentesque gravida dui, ac sagittis seque.',
            'answer' => 'Mauris in erat justo. Nullam ac urna eu felis dapibus condimentumi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus rhoncus alesuada. Pellentesque molestie lacus nec ligula rhoncus pretium.',
        ]
    ];

    public function load(ObjectManager $manager)
    {
        foreach ($this->feedbacks as $one) {
            $news = (new Feedback())
                ->setName($one['name'])
                ->setEmail($one['email'])
                ->setContent($one['content'])
                ->setAnswer($one['answer'])
            ;
            $manager->persist($news);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 14;
    }
}
