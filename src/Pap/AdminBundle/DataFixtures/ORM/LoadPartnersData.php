<?php

namespace Pap\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Pap\AdminBundle\Entity\Partner;

class LoadPartnersData extends AbstractFixture implements OrderedFixtureInterface
{
    private $partner = [
        [
            'title' => 'Форпост',
            'link' => 'www.forpost.by',
            'content' => 'Компания «Форпост» - это уже известный брэнд, заслуженно получивший признание и уважение потребителей.',
        ],
        [
            'title' => 'Альфа-Двери',
            'link' => 'www.alfa-dveri.ru',
            'content' => 'Компания «Альфа двери» - лидер по производству металлических дверей для домов и коттеджей из оцинкованной стали в Республике Польша.',
        ],
        [
            'title' => 'РосБелИнвест',
            'link' => 'www.rostbelinvest.by',
            'content' => 'Предприятие «РосБелИнвест» освоило уникальную технологию конструкции входных дверей «Магнат» из армированного стекловолокна, которые соединяют в себе эстетику дерева, легкость алюминия, прочность ПВХ и противовзломные характеристики стали.',
        ],
        [
            'title' => 'Грин-Плант',
            'link' => 'www.greenplant.by',
            'content' => 'Предприятие «Грин-Плант» - это благородство и долговечность, изысканность вкуса и высочайшее качество межкомнатных дверей из массива, которое достигается за счет использования самого современного оборудования и строгого соблюдения технологических норм.',
        ],
        [
            'title' => 'Еврострой',
            'link' => 'www.evrostroi.by',
            'content' => 'Предприятие «Еврострой» занимает ведущие позиции в производстве межкомнатных дверей из массива сосны в Гродненском регионе . �?х продукция зарекомендовала себя как оптимальное соотношение приемлемой цены и высокого качества, а также кратчайшие сроки изготовления.',
        ],
    ];

    public function load(ObjectManager $manager)
    {
        foreach ($this->partner as $one) {
            $partner = (new Partner())
                ->setTitle($one['title'])
                ->setLink($one['link'])
                ->setDescription($one['content'])
            ;
            $manager->persist($partner);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 12;
    }
}
