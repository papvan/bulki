<?php

namespace Pap\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Pap\AdminBundle\Entity\CatalogCategory;
use Pap\AdminBundle\Entity\CatalogProduct;
use Pap\AdminBundle\Form\CatalogCategoryType;
use Pap\AdminBundle\Form\CatalogProductType;

/**
 * Article controller.
 *
 * @Route("/catalog")
 */
class CatalogController extends Controller
{

    /**
     * Lists all Article entities.
     *
     * @Route("/{id}", name="admin_catalog_index", defaults={"id"=0})
     * @Method("GET")
     * @Template()
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id != 0) {
            $categories = $em->getRepository('PapAdminBundle:CatalogCategory')
                ->createQueryBuilder('catalog')
                ->where('catalog.parentId = :parent')
                ->setParameter('parent', $id)
                ->orderBy('catalog.position', 'ASC')
                ->getQuery()
                ->getResult()
            ;
            $products = $em->getRepository('PapAdminBundle:CatalogProduct')
                ->createQueryBuilder('catalog')
                ->where('catalog.parentId = :parent')
                ->setParameter('parent', $id)
                ->orderBy('catalog.position', 'ASC')
                ->getQuery()
                ->getResult()
            ;
            $parent = $em->getRepository('PapAdminBundle:CatalogCategory')->find($id);
        } else {
            $parent = null;
            $categories = $em->getRepository('PapAdminBundle:CatalogCategory')
                ->createQueryBuilder('cat')
                ->where('cat.parentId is null')
                ->orderBy('cat.position', 'ASC')
                ->getQuery()
                ->getResult()
            ;
            $products = $em->getRepository('PapAdminBundle:CatalogProduct')
                ->createQueryBuilder('cat')
                ->where('cat.parentId is null')
                ->orderBy('cat.position', 'ASC')
                ->getQuery()
                ->getResult()
            ;
        }

        //$this->exportProducts(); exit;
        return array(
            'categories' => $categories,
            'products' => $products,
            'id' => $id,
            'parent' => $parent,
        );
    }

    /**
     * Creates a new Article entity.
     *
     * @Route("/product/{parent}/create", name="admin_product_create", defaults={"parent"=0})
     * @Method("POST")
     * @Template("PapAdminBundle:Catalog:new.html.twig")
     */
    public function createAction(Request $request, $parent)
    {
        $em = $this->getDoctrine()->getManager();
        $entity  = new CatalogProduct();
        if ($parent != 0) {
            $oParent = $em->getRepository('PapAdminBundle:CatalogBase')->find($parent);
            $entity->setParent($oParent);
        }
        $form = $this->createForm(new CatalogProductType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $query = $em->getRepository('PapAdminBundle:CatalogProduct')
                ->createQueryBuilder('item')
                ->orderBy('item.position', 'DESC')
            ;
            if ($parent != 0){
                $query->where('item.parentId = :parent')
                    ->setParameter('parent', $parent)
                ;
            }

            $maxPosition = $query->getQuery()->getResult();

            if (count($maxPosition)){
                $entity->setPosition($maxPosition[0]->getPosition()+1);
            }
            else {
                $entity->setPosition(1);   
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_catalog_index', array('id' => $parent)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'parentId' => $parent,
            'parent' => $oParent,
        );
    }

    /**
     * Displays a form to create a new Article entity.
     *
     * @Route("/product/{parent}/new", name="admin_product_new", defaults={"parent"=0})
     * @Method("GET")
     * @Template()
     */
    public function newAction($parent)
    {
        $em = $this->getDoctrine()->getManager();
        $oParent = $em->getRepository('PapAdminBundle:CatalogCategory')->find($parent);
        $entity = new CatalogProduct();
        $form   = $this->createForm(new CatalogProductType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'parentId' => $parent,
            'parent' => $oParent,
        );
    }

    /**
     * Creates a new Article entity.
     *
     * @Route("/category/{parent}/create", name="admin_category_create", defaults={"parent"=0})
     * @Method("POST")
     * @Template("PapAdminBundle:Catalog:newCategory.html.twig")
     */
    public function createCategoryAction(Request $request, $parent)
    {
        $em = $this->getDoctrine()->getManager();
        $entity  = new CatalogCategory();
        if ($parent != 0) {
            $oParent = $em->getRepository('PapAdminBundle:CatalogBase')->find($parent);
            $entity->setParent($oParent);
        }
        $form = $this->createForm(new CatalogCategoryType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $query = $em->getRepository('PapAdminBundle:CatalogCategory')
                ->createQueryBuilder('item')
                ->orderBy('item.position', 'DESC')
            ;
            if ($parent != 0){
                $query->where('item.parentId = :parent')
                    ->setParameter('parent', $parent)
                ;
            }

            $maxPosition = $query->getQuery()->getResult();

            if (count($maxPosition)){
                $entity->setPosition($maxPosition[0]->getPosition()+1);
            }
            else {
                $entity->setPosition(1);   
            }

            $em->persist($entity);
            $em->flush();

            if ($entity->getParent())
                return $this->redirect($this->generateUrl('admin_catalog_index', ['id' => $entity->getParentId()]));
            else
                return $this->redirect($this->generateUrl('admin_catalog_index'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'parentId' => $parent,
            'parent' => $oParent,
        );
    }

    /**
     * Displays a form to create a new Article entity.
     *
     * @Route("/category/{parent}/new", name="admin_category_new", defaults={"parent"=0})
     * @Method("GET")
     * @Template()
     */
    public function newCategoryAction($parent)
    {
        $em = $this->getDoctrine()->getManager();
        $oParent = $em->getRepository('PapAdminBundle:CatalogCategory')->find($parent);
        $entity = new CatalogCategory();
        $form   = $this->createForm(new CatalogCategoryType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'parentId' => $parent,
            'parent' => $oParent,
        );
    }

    /**
     * Displays a form to edit an existing Article entity.
     *
     * @Route("/{id}/edit", name="admin_product_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PapAdminBundle:CatalogProduct')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $editForm = $this->createForm(new CatalogProductType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'parent' => $entity->getParent(),
        );
    }

    /**
     * Edits an existing Article entity.
     *
     * @Route("/{id}", name="admin_product_update")
     * @Method("POST")
     * @Template("PapAdminBundle:Catalog:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PapAdminBundle:CatalogProduct')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new CatalogProductType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            if ($entity->getParent())
                return $this->redirect($this->generateUrl('admin_catalog_index', ['id' => $entity->getParentId()]));
            else
                return $this->redirect($this->generateUrl('admin_catalog_index'));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'parent' => $entity->getParent(),
        );
    }

    /**
     * Displays a form to edit an existing Article entity.
     *
     * @Route("/category/{id}/edit", name="admin_category_edit")
     * @Method("GET")
     * @Template()
     */
    public function editCategoryAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PapAdminBundle:CatalogCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $editForm = $this->createForm(new CatalogCategoryType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'parent' => $entity->getParent(),
        );
    }

    /**
     * Edits an existing Article entity.
     *
     * @Route("/category/{id}", name="admin_category_update")
     * @Method("POST")
     * @Template("PapAdminBundle:Catalog:editCategory.html.twig")
     */
    public function updateCategoryAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PapAdminBundle:CatalogCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new CatalogCategoryType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            if ($entity->getParent())
                return $this->redirect($this->generateUrl('admin_catalog_index', ['id' => $entity->getParentId()]));
            else
                return $this->redirect($this->generateUrl('admin_catalog_index'));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'parent' => $entity->getParent(),
        );
    }

    /**
     * Deletes a Article entity.
     *
     * @Route("/delete/{id}", name="admin_catalog_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PapAdminBundle:CatalogBase')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Catalog entity.');
        }

        $parent = $entity->getParentId();

        $childs = $em->getRepository('PapAdminBundle:CatalogBase')->findByParentId($entity->getId());
        foreach ($childs as $value) {
            $em->remove($value);
        }
        $em->flush();

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_catalog_index', ['id'=>$parent]));
    }

    /**
     * Deletes a Article entity.
     *
     * @Route("/delete/checked/{parent}", name="admin_catalog_delete_checked", defaults={"parent":0})
     * @Method("POST")
     */
    public function deleteCheckedAction(Request $request, $parent)
    {
        if ($request->get('delete-items')) {
            $items = $request->get('delete-items');
            $em = $this->getDoctrine()->getManager();
            if (count($items)) {
                foreach ($items as $value) {
                    $entity = $em->getRepository('PapAdminBundle:CatalogBase')->find($value);
                    if ($entity) {
                                $childs = $em->getRepository('PapAdminBundle:CatalogBase')->findByParentId($entity->getId());
                                foreach ($childs as $value) {
                                    $em->remove($value);
                                }
                                $em->flush();
                        $em->remove($entity);
                    }
                }
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('admin_catalog_index', ['id'=>$parent]));
    }

    /**
     * @Route("/position-up/{id}", name="admin_catalog_position_up")
     * @Method("GET")
     */
    public function positionUpAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('PapAdminBundle:CatalogBase')->find($id);

        $query = $em->getRepository('PapAdminBundle:CatalogBase')
            ->createQueryBuilder('catalog')
        ;
        if ($item->getParent() == null) {
            $query->where('catalog.parent is null');
        } else {
            $query->where('catalog.parentId = :parent')
                ->setParameter('parent', $item->getParentId())
            ;
        }
        $secondItems = $query
            ->andWhere('catalog.position < :position')
            ->setParameter('position', $item->getPosition())
            ->orderBy('catalog.position', 'DESC')
            ->getQuery()
            ->getResult()
        ;

        if (count($secondItems)) {
            $sItem = $secondItems[0];
            $sItem->setPosition($sItem->getPosition()+1);
            $em->persist($sItem);
            $item->setPosition($item->getPosition()-1);
            $em->persist($item);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_catalog_index', ['id'=>$item->getParentId()]));
    }

    /**
     * @Route("/position-down/{id}", name="admin_catalog_position_down")
     * @Method("GET")
     */
    public function positionDownAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('PapAdminBundle:CatalogBase')->find($id);
        $query = $em->getRepository('PapAdminBundle:CatalogBase')
            ->createQueryBuilder('catalog')
        ;
        if ($item->getParent() == null) {
            $query->where('catalog.parent is null');
        } else {
            $query->where('catalog.parentId = :parent')
                ->setParameter('parent', $item->getParentId())
            ;
        }
        $secondItems = $query
            ->andWhere('catalog.position > :position')
            ->setParameter('position', $item->getPosition())
            ->orderBy('catalog.position', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        if (count($secondItems)) {
            $sItem = $secondItems[0];
            $sItem->setPosition($sItem->getPosition()-1);
            $em->persist($sItem);
            $item->setPosition($item->getPosition()+1);
            $em->persist($item);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_catalog_index', ['id'=>$item->getParentId()]));
    }

    /**
     * @Route("/position/resset", name="admin_catalog_position_resset")
     * @Method("GET")
     */
    public function ressetPositionsAction()
    {
        $this->recurse();

        return $this->redirect($this->generateUrl('admin_catalog_index'));
    }

    private function recurse($id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $position = 1;
        $items = $em->getRepository('PapAdminBundle:CatalogBase')->findByParentId($id);
        foreach ($items as $item) {
            $item->setPosition($position);
            $em->persist($item);
            $position++;
            $this->recurse($item->getId());
        }
        $em->flush();

        return true;
    }

    /**
     * Creates a form to delete a Article entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    private function exportProducts()
    {
        $products = $this->getDoctrine()->getManager()->getRepository('PapAdminBundle:CatalogProduct')->findAll();
        $aProducts = [];
        print "Query Products = " . count($products).'<br>';
        foreach ($products as $value) {
            if ($value->getParent()->getDoors()) {
                $aProducts['category'.$value->getParent()->getId()][] = $value;
            }
        }

        $sumCount = 0;
        foreach ($aProducts as $key => $one) {
            //$file = fopen('../../../../web/images/'.$key.'.txt', 'w+');
            $sumCount += count($one);
            $string = '';
            print '<html>
                    <head>
                        <meta charset="UTF-8" />
                    </head>
                    <body>';
            print "-------------------------------------------".$key."------------------<br>";
            foreach ($one as $name => $item) {
                    $string .=
                        '"'.$name.'" => [<br>'.
                            '"type"  => "product",<br>'.
                            '"title" => "'.$item->getTitle().'",<br>'.
                            '"isDoor" => true,<br>'.
                            '"price" => '. $item->getPrice().',<br>'.
                            '"company" => "'.$item->getCompany().'",<br>'.
                            '"material" => "'.$item->getMaterial().'",<br>'.
                            '"content" => \''.htmlspecialchars($item->getContent()).'\',<br>'.
                            '"description" => \''.htmlspecialchars($item->getDescription()).'\',<br>'.
                            '"image" => "'.$item->getImageName().'",<br>'.
                        '],<br>'
                    ;
            }
            print $string;
            /*$result = fwrite($file, $string);
            if ($result) {
                print "File Wrote";
            } else {
                print "Error with file writing!!!";
            }
            fclose($file);*/
        }
        print "Products = " . $sumCount.'<br>';
        exit;
    }
}
