<?php

namespace Pap\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Pap\AdminBundle\Entity\Order;
use Pap\AdminBundle\Form\NewsType;

/**
 * Order controller.
 *
 * @Route("/order")
 */
class OrderController extends Controller
{

    /**
     * @Route("/", name="admin_order_all")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$orders = $em->getRepository('PapAdminBundle:Order')
    		->createQueryBuilder('orders')
    		->orderBy('orders.date', 'DESC')
    		->getQuery()
    		->getResult()
    	;

    	return [
    		'entities' => $orders,
    		'statuses' => ['new'=>Order::STATUS_NEW, 'open'=>Order::STATUS_OPEN, 'closed'=>Order::STATUS_CLOSED],
    	];
    }

    /**
     * @Route("/show/{id}", name="admin_order_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    	$order = $em->getRepository('PapAdminBundle:Order')->find($id);

    	return [
    		'entity' => $order,
    		'statuses' => ['new'=>Order::STATUS_NEW, 'open'=>Order::STATUS_OPEN, 'closed'=>Order::STATUS_CLOSED],
    	];
    }

    /**
     * @Route("/show/{id}/delete", name="admin_order_delete")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    	$order = $em->getRepository('PapAdminBundle:Order')->find($id);
    	foreach ($order->getProducts() as $value) {
    		$em->remove($value);
    	}
    	$em->remove($order);
    	$em->flush();

    	return $this->redirect($this->generateUrl('admin_order_all'));
    }

    /**
     * @Route("/change", name="admin_order_change_status")
     * @Method("POST")
     */
    public function changeAction(Request $request)
    {
    	if ($request->get('orderNumber') && $request->get('newStatus')) {
    		$number = $request->get('orderNumber');
    		$status = $request->get('newStatus');
    		$em = $this->getDoctrine()->getManager();
    		$order = $em->getRepository('PapAdminBundle:Order')->findOneByNumber($number);
    		if (!$order) {
    			throw $this->createNotFoundException("Order #".$number." Not Found");
    		}

    		$order->setStatus($status);
    		$order->setLooked(true);
    		$em->persist($order);
    		$em->flush();

    		if ($request->get('action')) {
    			return $this->redirect($this->generateUrl($request->get('action')));
    		} else {
    			return $this->redirect($this->generateUrl('admin_order_show', array('id'=>$order->getId())));
    		}
    	} else {
    		throw $this->createNotFoundException("No Parameters");
    	}
    }
}