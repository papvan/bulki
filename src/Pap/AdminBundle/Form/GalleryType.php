<?php

namespace Pap\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GalleryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Заголовок',
            ])
            ->add('imageSmall', null, [
                'label' => 'Маленькое изображение',
            ])
            ->add('image', null, [
                'label' => 'Изображение',
            ])
            ->add('position', null, [
                'label' => 'Позиция (При добавлении продукции можно не указывать, заполняется автоматически)',
                'required' => false,
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Pap\AdminBundle\Entity\Gallery'
        ));
    }

    public function getName()
    {
        return 'pap_adminbundle_gallery';
    }
}
