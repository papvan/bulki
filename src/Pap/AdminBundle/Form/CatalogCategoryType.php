<?php

namespace Pap\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CatalogCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Заголовок',
            ])
            // ->add('doors', null, [
            //     'label' => 'Категория дверей?',
            //     'required' => false,
            // ])
            ->add('description', 'genemu_tinymce', [
                'label' => 'Описание категории',
                'configs' => [
                    'entity_encoding' => "raw",
                    'language' => 'ru',
                    'menubar' => false,
                    'statusbar' => false,
                    'resize' => false,
                    'width' =>'100%',
                    'height' => 250,
                    'plugins' => ['image', 'link', 'code', 'paste', 'emoticons'],
                    'toolbar1' => 'undo redo | bold italic underline | bullist numlist | link image ',
                ],
            ])
            ->add('position', null, [
                'label' => 'Позиция (При добавлении категории можно не указывать, заполняется автоматически)',
                'required' => false,
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Pap\AdminBundle\Entity\CatalogCategory'
        ));
    }

    public function getName()
    {
        return 'pap_adminbundle_catlogcategory';
    }
}
