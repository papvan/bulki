<?php

namespace Pap\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PartnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Заголовок'
            ])
            ->add('description', 'textarea', [
                'label' => 'Описание',
            ])
            ->add('link', null, [
                'label' => 'Ссылка'
            ])
            ->add('image', null, [
                'label' => 'Изображение',
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Pap\AdminBundle\Entity\Partner'
        ));
    }

    public function getName()
    {
        return 'pap_adminbundle_partnertype';
    }
}
