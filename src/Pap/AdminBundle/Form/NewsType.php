<?php

namespace Pap\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Заголовок',
            ])
            ->add('brief', 'genemu_tinymce', [
                'label' => 'Краткое описание',
                'configs' => [
                    'entity_encoding' => "raw",
                    'language' => 'ru',
                    'menubar' => false,
                    'statusbar' => false,
                    'resize' => false,
                    'width' =>'100%',
                    'height' => 250,
                    'plugins' => ['image', 'link', 'code', 'paste', 'emoticons'],
                    'toolbar1' => 'undo redo | bold italic underline | bullist numlist | link image ',
                ],
            ])
            ->add('description', 'genemu_tinymce', [
                'label' => 'Полное описание',
                'configs' => [
                    'entity_encoding' => "raw",
                    'language' => 'ru',
                    'menubar' => false,
                    'statusbar' => false,
                    'resize' => false,
                    'width' =>'100%',
                    'height' => 250,
                    'plugins' => ['image', 'link', 'code', 'paste', 'emoticons'],
                    'toolbar1' => 'undo redo | bold italic underline | bullist numlist | link image ',
                ],
            ])
            ->add('image', null, [
                'label' => 'Изображение',
            ])
            ->add('metaTitle', null, [
                'label' => 'META Заголовок',
            ])
            ->add('metaKeywords', null, [
                'label' => 'МЕТА Ключевые слова',
            ])
            ->add('metaDescription', null, [
                'label' => 'МЕТА Описание',
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Pap\AdminBundle\Entity\News'
        ));
    }

    public function getName()
    {
        return 'pap_adminbundle_newstype';
    }
}
