<?php

namespace Pap\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CatalogProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'Заголовок',
            ])
            ->add('price', null, [
                'label' => 'Цена',
            ])
            ->add('description', 'genemu_tinymce', [
                'label' => 'Описание',
                'configs' => [
                    'entity_encoding' => "raw",
                    'language' => 'ru',
                    'menubar' => false,
                    'statusbar' => false,
                    'resize' => false,
                    'width' =>'100%',
                    'height' => 250,
                    'plugins' => ['image', 'link', 'code', 'paste', 'emoticons'],
                    'toolbar1' => 'undo redo | bold italic underline | bullist numlist | link image ',
                ],
            ])
            ->add('content', 'genemu_tinymce', [
                'label' => 'Характеристики',
                'configs' => [
                    'entity_encoding' => "raw",
                    'language' => 'ru',
                    'menubar' => false,
                    'statusbar' => false,
                    'resize' => false,
                    'width' =>'100%',
                    'height' => 250,
                    'plugins' => ['image', 'link', 'code', 'paste', 'emoticons'],
                    'toolbar1' => 'undo redo | bold italic underline | bullist numlist | link image ',
                ],
            ])
            ->add('image', null, [
                'label' => 'Изображение',
            ])
            ->add('promo', null, [
                'label' => 'Акция',
                'required' => false,
            ])
            ->add('hit', null, [
                'label' => 'Топ продаж',
                'required' => false,
            ])
            ->add('popular', null, [
                'label' => 'Новинка',
                'required' => false,
            ])
            ->add('roznica', null, [
                'label' => 'Розница',
                'required' => false,
            ])
            ->add('opt', null, [
                'label' => 'Опт',
                'required' => false,
            ])
            ->add('position', null, [
                'label' => 'Позиция (При добавлении продукции можно не указывать, заполняется автоматически)',
                'required' => false,
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Pap\AdminBundle\Entity\CatalogProduct'
        ));
    }

    public function getName()
    {
        return 'pap_adminbundle_catlogproduct';
    }
}
