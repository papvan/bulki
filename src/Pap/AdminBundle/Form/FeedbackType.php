<?php

namespace Pap\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FeedbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'ФИО',
            ])
            ->add('email', 'email', [
                'label' => 'Email',
            ])
            ->add('content', null, [
                'label' => 'Сообщение',
            ])
            ->add('answer', null, [
                'label' => 'Ответ',
            ])
            ->add('hidden', null, [
                'label' => 'Скрыть',
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Pap\AdminBundle\Entity\Feedback'
        ));
    }

    public function getName()
    {
        return 'pap_adminbundle_feedbacktype';
    }
}
