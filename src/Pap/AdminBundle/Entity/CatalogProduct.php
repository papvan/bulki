<?php

namespace Pap\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use Pap\AdminBundle\Entity\CatalogBase as Base;

/**
 * Partner
 *
 * @ORM\Table(name="catalog")
 * @ORM\Entity(repositoryClass="Pap\AdminBundle\Entity\Repository\CatalogRepository")
 * @Vich\Uploadable
 */
class CatalogProduct extends Base
{
    /**
     * @var price
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var price
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var price
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @Assert\File(
     *     maxSize="3M",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
     * )
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName")
     *
     * @var File $image
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=255, name="image_name")
     *
     * @var string $imageName
     */
    protected $imageName;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;     

    /**
     * @ORM\Column(name="is_popular", type="boolean", nullable=true)
     */
    private $popular;

    /**
     * @ORM\Column(name="is_hit", type="boolean", nullable=true)
     */
    private $hit;

    /**
     * @ORM\Column(name="is_promo", type="boolean", nullable=true)
     */
    private $promo; 

    /**
     * @ORM\Column(name="is_opt", type="boolean", nullable=true)
     */
    private $opt;

    /**
     * @ORM\Column(name="is_roznica", type="boolean", nullable=true)
     */
    private $roznica;

    public function __construct()
    {
        $this->door = true;
        $this->roznica = true;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return CatalogProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CatalogProduct
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return CatalogProduct
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     * @return Partner
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    
        return $this;
    }

    /**
     * Get imageName
     *
     * @return string 
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    public function setImage($image)
    {
        $this->image = $image;
        if ($image instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Partner
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setPopular($popular)
    {
        $this->popular = $popular;

        return $this;
    }

    public function isPopular()
    {
        return $this->popular;
    }

    public function setHit($hit)
    {
        $this->hit = $hit;

        return $this;
    }

    public function isHit()
    {
        return $this->hit;
    }

    public function setPromo($promo)
    {
        $this->promo = $promo;

        return $this;
    }

    public function isPromo()
    {
        return $this->promo;
    }

    public function setOpt($opt)
    {
        $this->opt = $opt;

        return $this;
    }

    public function isOpt()
    {
        return $this->opt;
    }

    public function setRoznica($roznica)
    {
        $this->roznica = $roznica;

        return $this;
    }

    public function isRoznica()
    {
        return $this->roznica;
    }
}