<?php

namespace Pap\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Order
 *
 * @ORM\Table(name="order_products")
 * @ORM\Entity(repositoryClass="Pap\AdminBundle\Entity\Repository\CatalogRepository")
 * @Vich\Uploadable
 */
class OrderProduct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="products")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=true)
     */
    private $orderId;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogProduct")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\Column(name="product_id", type="integer", nullable=true)
     */
    private $productId;

    /**
     * @ORM\Column(name="count", type="integer")
     */
    private $count;

    public function __construct($order = null, $product = null)
    {
        if ($order != null) {
            $this->setOrder($order);
        }

        if ($product != null) {
            $this->setProduct($product);
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     * @return OrderProduct
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return OrderProduct
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set order
     *
     * @param \Pap\AdminBundle\Entity\Order $order
     * @return OrderProduct
     */
    public function setOrder(\Pap\AdminBundle\Entity\Order $order = null)
    {
        $this->order = $order;
        $this->setOrderId($order->getId());

        return $this;
    }

    /**
     * Get order
     *
     * @return \Pap\AdminBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set product
     *
     * @param \Pap\AdminBundle\Entity\CatalogProduct $product
     * @return OrderProduct
     */
    public function setProduct(\Pap\AdminBundle\Entity\CatalogProduct $product = null)
    {
        $this->product = $product;
        $this->setProductId($product->getId());

        return $this;
    }

    /**
     * Get product
     *
     * @return \Pap\AdminBundle\Entity\CatalogProduct 
     */
    public function getProduct()
    {
        return $this->product;
    }

    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    public function getCount()
    {
        return $this->count;
    }
}
