<?php

namespace Pap\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Pap\AdminBundle\Entity\CatalogBase as Base;

/**
 * Partner
 *
 * @ORM\Table(name="catalog")
 * @ORM\Entity(repositoryClass="Pap\AdminBundle\Entity\Repository\CatalogRepository")
 */
class CatalogCategory extends Base
{
    /**
     * @var title
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var isDoor
     *
     * @ORM\Column(name="is_doors", type="boolean", nullable=true)
     */
    private $doors;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    public function __construct()
    {
        $this->doors = true;
    }

    /**
     * Set name
     *
     * @param  string          $name
     * @return CatalogCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set doors
     *
     * @param  boolean         $doors
     * @return CatalogCategory
     */
    public function setDoors($doors)
    {
        $this->doors = $doors;

        return $this;
    }

    /**
     * Get doors
     *
     * @return boolean
     */
    public function getDoors()
    {
        return $this->doors;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }
}
