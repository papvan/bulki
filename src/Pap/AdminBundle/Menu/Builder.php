<?php
namespace Pap\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->setChildrenAttribute('class', 'nav');

        $menu->addChild('Гродвита Плюс', array('route' => 'content_home'));
        $menu->addChild('Заказы', array('route' => 'admin_order_all'));
        $menu->addChild('Страницы', array('route' => 'page'));
        $menu->addChild('О компании', array('route' => 'partner'));
        $menu->addChild('Каталог', array('route' => 'admin_catalog_index'));
        $menu->addChild('Новости', array('route' => 'news'));
        $menu->addChild('Отзывы', array('route' => 'feedback'));
        $menu->addChild('Словари', array('route' => 'pap_dictionary'));

        return $menu;
    }
}