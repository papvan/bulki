<?php

namespace Pap\ContentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Pap\PageBundle\Controller\MainController as Controller;

use Pap\AdminBundle\Entity\Partner;

class AboutController extends Controller
{
    protected $_page = 'about';
    /**
     * @Route("/about", name="content_about")
     * @Template()
     */
    public function indexAction()
    {
        return [
        	'partners' => $this->getDoctrine()->getManager()->getRepository('PapAdminBundle:Partner')->findAll(),
            'page' => $this->getPage(),
        ];
    }
}
