<?php

namespace Pap\ContentBundle\Controller;

use Pap\PageBundle\Controller\MainController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Pap\AdminBundle\Entity\News;

class NewsController extends Controller
{
    protected $_page = 'news';

    /**
     * @Route("/news", name="content_news")
     * @Template()
     */
    public function indexAction()
    {
        $em    = $this->getDoctrine()->getManager();
        $query = $em->getRepository('PapAdminBundle:News')
            ->createQueryBuilder('news')
            ->orderBy('news.date', 'DESC')
        ;
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1)/*page number*/,
            4/*limit per page*/
        );

        return [
            'news' => $pagination,
            'page' => $this->getPage(),
        ];
    }

    /**
     * @Route("/news/{id}", name="content_news_show")
     * @Template()
     */
    public function showAction($id)
    {
        $entity = $this->getDoctrine()->getManager()->getRepository('PapAdminBundle:News')->find($id);
        if (trim($entity->getMetaTitle()) != '') {
            $page['metaTitle'] = $entity->getMetaTitle();
        } else {
            $page['metaTitle'] = $entity->getTitle();
        }
        if (trim($entity->getMetaKeywords()) != '') {
            $page['metaKeywords'] = $entity->getMetaKeywords();
        } else {
            $page['metaKeywords'] = implode(',', explode(' ', $page['metaTitle']));
        }
        $page['metaDescription'] = $entity->getMetaDescription();

        return [
            'entity' => $entity,
            'page' => $page,
        ];
    }

    /**
     * @Route("/news/last", name="content_news_show_last")
     * @Template("PapContentBundle:News:showLast.html.twig")
     */
    public function showLastAction()
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('PapAdminBundle:News')
            ->createQueryBuilder('news')
            ->orderBy('news.date', 'DESC')
            ->setMaxResults(2)
            ->getQuery()
            ->getResult()
        ;

        return [
            'news' => $news,
        ];
    }
}
