<?php

namespace Pap\ContentBundle\Controller;

use Pap\PageBundle\Controller\MainController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Pap\AdminBundle\Entity\Feedback;
use Pap\ContentBundle\Form\FeedbackType;

class FeedbackController extends Controller
{
    protected $_page = 'feedback';

    /**
     * @Route("/feedbacks", name="content_feedbacks")
     * @Template()
     */
    public function indexAction()
    {
        $em    = $this->getDoctrine()->getManager();
        $query = $em->getRepository('PapAdminBundle:Feedback')
            ->createQueryBuilder('feedback')
            ->orderBy('feedback.id', 'DESC')
            ->where('feedback.hidden = 0 OR feedback.hidden is NULL')
        ;
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1)/*page number*/,
            4/*limit per page*/
        );

        $form = $this->createForm(new FeedbackType(), new Feedback);

        return [
            'feedbacks' => $pagination,
            'form' => $form->createView(),
            'page' => $this->getPage(),
        ];
    }

    /**
     * @Route("/feedbacks/new", name="content_feedbacks_new")
     * @Template("PapContentBundle:Feedback:index.html.twig")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        $em    = $this->getDoctrine()->getManager();
        $query = $em->getRepository('PapAdminBundle:Feedback')
            ->createQueryBuilder('feedback')
            ->orderBy('feedback.id', 'DESC')
        ;
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1)/*page number*/,
            4/*limit per page*/
        );

        $feedback = new Feedback;
        $form = $this->createForm(new FeedbackType(), $feedback);
        $form->bind($request);

        if ($form->isValid()){
            $em->persist($feedback);
            $em->flush();

            return $this->redirect($this->generateUrl('content_feedbacks'));
        }

        return [
            'feedbacks' => $pagination,
            'form' => $form->createView(),
            'page' => $this->getPage(),
        ];
    }
}
