<?php

namespace Pap\ContentBundle\Controller;

use Pap\PageBundle\Controller\MainController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class HomeController extends Controller
{
	protected $_page = 'home';

    /**
     * @Route("/", name="content_home")
     * @Template()
     */
    public function indexAction()
    {
        return [
        	'page' => $this->getPage(),
        ];
    }
}
