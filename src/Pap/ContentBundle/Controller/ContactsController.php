<?php

namespace Pap\ContentBundle\Controller;

use Pap\PageBundle\Controller\MainController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Pap\AdminBundle\Entity\Feedback;
use Pap\ContentBundle\Form\FeedbackType;

class ContactsController extends Controller
{
    protected $_page = 'contacts';

    /**
     * @Route("/contacts", name="content_contacts")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->createForm(new FeedbackType(), new Feedback);

        return [
            'form' => $form->createView(),
            'page' => $this->getPage(),
        ];
    }

    /**
     * @Route("/contacts/send", name="content_contacts_new")
     * @Template("PapContentBundle:Contacts:index.html.twig")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        $feedback = new Feedback;
        $form = $this->createForm(new FeedbackType(), $feedback);
        $form->bind($request);

        if ($form->isValid()){

            $message = \Swift_Message::newInstance()
                ->setSubject('Сообщение с сайта')
                ->setFrom($feedback->getEmail())
                ->setTo($this->container->getParameter('email_for_contacts'))
                ->setBody(
                    $this->renderView(
                        'PapContentBundle:Contacts:email.html.twig',
                        array('data' => $feedback)
                    )
                )
            ;
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('content_contacts'));
        }

        return [
            'form' => $form->createView(),
            'page' => $this->getPage(),
        ];
    }
}
