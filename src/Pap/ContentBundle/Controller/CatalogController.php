<?php

namespace Pap\ContentBundle\Controller;

use Pap\PageBundle\Controller\MainController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Pap\AdminBundle\Entity\CatalogCategory;
use Pap\AdminBundle\Entity\CatalogProduct;

class CatalogController extends Controller
{
    protected $_page = 'catalog';

    /**
     * @Route("/catalog/{id}/{type}", name="content_catalog", defaults={"id"=null, "type"=0})
     * @Template()
     */
    public function indexAction($id, $type)
    {
        $em = $this->getDoctrine()->getManager();

        if ($id != null) {
            $parent = $em->getRepository('PapAdminBundle:CatalogCategory')->findOneById($id);
        }

        $query = $em->getRepository('PapAdminBundle:CatalogProduct')
            ->createQueryBuilder('product')
            ->orderBy('product.promo', 'DESC')
            ->addOrderBy('product.hit', 'DESC')
            ->addOrderBy('product.id', 'ASC')
            ->addOrderBy('product.title', 'ASC')
        ;

        if (isset($parent) && $parent) {
            $query->where('product.parentId = :parent')
                ->setParameter('parent', $parent->getId())
            ;
        } else {
            $parent = null;
        }

        if ($type) {
            $query->andWhere('product.opt = 1');
        } else {
            $query->andWhere('product.roznica = 1');
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1)/*page number*/,
            4/*limit per page*/
        );

        return [
            'products' => $pagination,
            'parent' => $parent,
            'type' => $type,
            'page' => $this->getPage(),
        ];
    }

    /**
     * @Route("/catalog-product/{id}/{type}", name="content_catalog_show_product", defaults={"type"=0})
     * @Template()
     */
    public function showAction($id, $type)
    {
        return [
            'entity' => $this->getDoctrine()->getManager()->getRepository('PapAdminBundle:CatalogProduct')->find($id),
            'type' => $type,
        ];
    }

    /**
     * @Route("/catalog/popular", name="content_catalog_show_popular")
     * @Template()
     */
    public function showPopularAction()
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('PapAdminBundle:CatalogProduct')
            ->createQueryBuilder('product')
            ->where('product.popular = true')
            //->setMaxResults(4)
            ->getQuery()
            ->getResult()
        ;

        return [
            'products' => $products,
        ];
    }

    /**
     * @Route("/catalog/categories/{parent}/{type}", name="content_catalog_categories", defaults={"parent"=0, "type"=0})
     * @Template()
     */
    public function categoriesAction($parent, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('PapAdminBundle:CatalogCategory')
            ->createQueryBuilder('category')
            ->where('category.parent is null')
            ->orderBy('category.position', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        if ($parent) {
            $parent = $em->getRepository('PapAdminBundle:CatalogCategory')->find($parent);
        } else {
            $parent = null;
        }

        return [
            'categories' => $categories,
            'parent' => $parent,
            'type' => $type,
        ];
    }

    /**
     * @Route("/catalog/categories/children/{id}/{parent}/{type}", name="content_catalog_categories_children", defaults={"parent"=0, "id"=0, "type"=0})
     * @Template("PapContentBundle:Catalog:categories_recurse.html.twig")
     */
    public function categoryChildrensAction($id, $parent = 0, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('PapAdminBundle:CatalogCategory')
            ->createQueryBuilder('category')
            ->where('category.parentId = :parent')
            ->setParameter('parent', $id)
            ->orderBy('category.position', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        if ($parent != 0)
            $oParent = $em->getRepository('PapAdminBundle:CatalogCategory')->find($parent);
        else
            $oParent = null;

        return [
            'categories' => $categories,
            'parent' => $oParent,
            'type' => $type,
        ];
    }
}

