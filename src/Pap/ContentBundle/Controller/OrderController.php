<?php

namespace Pap\ContentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Pap\PageBundle\Controller\MainController as Controller;
use Symfony\Component\Form\FormError;

use Pap\AdminBundle\Entity\CatalogProduct;
use Pap\AdminBundle\Entity\Order;
use Pap\AdminBundle\Entity\OrderProduct;
use Pap\ContentBundle\Form\OrderType;

class OrderController extends Controller
{
    protected $_page = 'order';
    
    /**
     * @Route("/order", name="content_order")
     * @Template()
     */
    public function indexAction()
    {
        return [
        	'products' => $this->getDoctrine()->getManager()->getRepository('PapAdminBundle:CatalogProduct')
                ->createQueryBuilder('pr')
                ->orderBy('pr.title', 'ASC')
                ->getQuery()
                ->getResult()
            ,
            'form' => $this->createForm(new OrderType, new Order)->createView(),
            'page' => $this->getPage(),
        ];
    }

    /**
     * @Route("/order/check", name="content_order_check")
     * @Template()
     */
    public function checkAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();   
        $order = new Order();
        do {
            $number = rand(1000, 100000000);
        } while ($em->getRepository('PapAdminBundle:Order')->findOneByNumber($number)); 
        $order->setNumber($number);
        $form = $this->createForm(new OrderType, $order);
        $form->bind($request);

        if ($form->isValid()) {
            if (count($request->get('product_count'))) {
                $em->persist($order);
                $products = $request->get('product_count');
                $flag = false;
                foreach ($products as $key => $value) {
                    if ($value > 0) {
                        $flag = true;
                        $prodct = $em->getRepository('PapAdminBundle:CatalogProduct')->find($key);
                        $op = new OrderProduct($order, $prodct);
                        $op->setCount($value);
                        $em->persist($op);
                    }
                }
                if ($flag) {
                    $em->flush();
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Заказ оформлен')
                        ->setFrom($this->container->getParameter('email_for_contacts'))
                        ->setTo($order->getEmail())
                        ->setBody(
                            $this->renderView(
                                'PapContentBundle:Order:print.html.twig',
                                array('order' => $order)
                            )
                        )
                    ;
                    $this->get('mailer')->send($message);

                    return $this->redirect($this->generateUrl('content_order_access', array(
                        'number' => $order->getNumber()
                    )));
                } else {
                    $form->addError(new FormError('Укажите количество продуктов, желаемых приобрести'));
                }
            }
        }

        return $this->render(
            'PapContentBundle:Order:index.html.twig',
            array(
                'products' => $this->getDoctrine()->getManager()->getRepository('PapAdminBundle:CatalogProduct')
                ->createQueryBuilder('pr')
                ->orderBy('pr.title', 'ASC')
                ->getQuery()
                ->getResult()
                ,
                'form' => $form->createView(),
                'page' => $this->getPage(),
            )
        );
    }

    /**
     * @Route("/order/{number}/access", name="content_order_access")
     * @Template()
     */
    public function accessAction($number)
    {
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository('PapAdminBundle:Order')->findOneByNumber($number);
        if (!$order) {
            throw $this->createNotFoundException("Order Not Found");
        }

        return [
            'order' => $order,
            'page' => $this->getPage(),
        ];
    }

    /**
     * @Route("/order/{number}/print", name="content_order_print")
     * @Template()
     */
    public function printAction($number)
    {
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository('PapAdminBundle:Order')->findOneByNumber($number);
        if (!$order) {
            throw $this->createNotFoundException("Order Not Found");
        }

        return [
            'order' => $order,
        ];
    }
}
