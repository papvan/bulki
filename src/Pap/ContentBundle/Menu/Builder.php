<?php
namespace Pap\ContentBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');


        $menu->setChildrenAttribute('id', 'nav');

        $menu->addChild('Главная', array('route' => 'content_home'));
        $menu->addChild('О компании', array('route' => 'content_about'));
        $catalog = $menu->addChild('Каталог', array('route' => 'content_catalog'));
        $catalog->addChild('Розница', array('route' => 'content_catalog'));
        $catalog->addChild('Опт', array(
            'route' => 'content_catalog', 
            'routeParameters'=> array(
                'id' => 0,
                'type'=>1,
            ),
        ));
        $menu->addChild('Заказать', array('route' => 'content_order'));
        $menu->addChild('Новости', array('route' => 'content_news'));
        $menu->addChild('Отзывы', array('route' => 'content_feedbacks'));
        $menu->addChild('Контакты', array('route' => 'content_contacts'));

        return $menu;
    }

    public function footerMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->setChildrenAttribute('id', 'footNav');

        $menu->addChild('Главная', array('route' => 'content_home'));
        $menu->addChild('О компании', array('route' => 'content_about'));
        $menu->addChild('Каталог', array('route' => 'content_catalog'));
        $menu->addChild('Заказать', array('route' => 'content_order'));
        $menu->addChild('Новости', array('route' => 'content_news'));
        $menu->addChild('Отзывы', array('route' => 'content_feedbacks'));
        $menu->addChild('Контакты', array('route' => 'content_contacts'));

        return $menu;
    }
}