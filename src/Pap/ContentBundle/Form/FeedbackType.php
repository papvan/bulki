<?php

namespace Pap\ContentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FeedbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'text',
                ]
            ])
            ->add('email', 'email', [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'text',
                ]
            ])
            ->add('content', 'textarea', [
                'label' => false,
                'required' => true,
                'attr' => [
                    'rows' => 5,
                    'cols' => 45,
                    'class' => 'textbody',
                ]
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Pap\AdminBundle\Entity\Feedback'
        ));
    }

    public function getName()
    {
        return 'pap_adminbundle_feedbacktype';
    }
}
