<?php

namespace Pap\ContentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'text',
                ]
            ])
            ->add('phone', null, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'text',
                ]
            ])
            ->add('address', null, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'text',
                ]
            ])
            ->add('email', 'email', [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'text',
                ]
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Pap\AdminBundle\Entity\Order'
        ));
    }

    public function getName()
    {
        return 'pap_adminbundle_order';
    }
}
