<?php

namespace Pap\PageBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Main controller.
 *
 */
class MainController extends Controller
{
    protected $_page = 'home';

    public function getAction($name)
    {
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('PapPageBundle:Page')->findOneByName($name);

        if (!$page) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        return array('page' => $page);
    }

    protected function getPage()
    {
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('PapPageBundle:Page')->findOneByName($this->_page);

        if (!$page) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $result = array();
        $result['title'] = $page->getTitle();
        $result['content'] = $page->getContent();
        if ($page->getMetaTitle() == '') {
            $result['metaTitle'] = $page->getTitle();
        } else {
            $result['metaTitle'] = $page->getMetaTitle();
        }

        if (trim($page->getMetaKeywords()) == '') {
            $result['metaKeywords'] = implode(',',explode(' ', $result['metaTitle']));
        } else {
            $result['metaKeywords'] = $page->getMetaKeywords();
        }
        $result['metaDescription'] = $page->getMetaDescription();

        return $result;
    }
}
