<?php

namespace Pap\DictionaryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DictionaryController extends Controller
{
    public function getByNameAction($name)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $oDictionary = $em->getRepository('PapDictionaryBundle:Dictionary')->findOneByName($name);
      
        return $this->render('PapDictionaryBundle:Dictionary:index.html.twig', array('value' => $oDictionary->getValue()));
    }
}
